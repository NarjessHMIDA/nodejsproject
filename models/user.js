

const mongoose = require('mongoose');
const movie = require('./movies');

var UserSchema = new mongoose.Schema({
  name: {
    type : String,
  required : 'this field is required'
  
  },
  age : Number,
  movies : [{
    type: mongoose.Schema.Types.Mixed,
    ref: 'movie'
  }]
});

UserSchema.virtual('countMovies').get(function (){
  return  this.movies.length;
});

UserSchema.pre('remove', function (next) {
  Movie.remove(_id, { $in : this.movies}).then ( () =>{
    next();
  })
})


const User = mongoose.model('user', UserSchema);

module.exports = User;