require ('./models/db');
require ('./models/user.js');
require ('./models/movies.js');


const usercontroller = require('./controllers/user_controller.js');
const express = require ('express');
const app = express();
const path = require ('path');
const exphbs = require ('express-handlebars');
const bodyparser = require('body-parser');

app.use(bodyparser.urlencoded({
    extended: true
}));

app.use(bodyparser.json());


app.set('views', path.join(__dirname, '/views/'));

app.engine('hbs', exphbs({ extname: 'hbs', defaultLayout: 'mainLayout', layoutsDir: __dirname + '/views/layouts'}));

app.set('view engine', 'hbs');

app.listen(3000, ()=>console.log('server has start on port 3000....'));


app.use('/user', usercontroller);