const express = require('express');
var router = express.Router();
const mongoose = require('mongoose');
const User = mongoose.model('user');


router.get('/', (req, res) =>{
res.render("user/addOredit", {
    viewTitle : "Insert user"
});
}) ;

router.post('/', (req, res) =>{
    if(req.body._id=='')
      insertUser(req, res) ;
    else
    updateUser(req, res) ;
 }) ;
    

  function insertUser(req, res)  {
      var user = new User ();
      user.name = req.body.name;
      user.age= req.body.age;
      user.movies = req.body.movies;
      user.save((err, doc)=> {
          if(!err)
          res.redirect('user/list');
          else {
              console.log ('error during insert user' + err);
          }
      });
  }

  function updateUser(req, res){
      User.findByIdAndUpdate({ _id: req.body._id}, req.body, {new : true}, (err, doc) =>{
        if(!err){
            res.redirect('user/list'); }
            else {
                res.render("user/addOredit", {
                    viewTitle: 'Update user',
                    user : req.body
                });
            }
      });
  }
  router.get('/list', (req, res)=> {
     User.find((err, docs) =>{
        if (!err){
            res.render("user/list", {
                list : docs
            });
        }
        else {
            console.log('Error in retrieing user list :' + err);
        }
     });
  });
router.get('/:id', (req, res)=>{
User.findById(req.params.id, (err, doc) => {
    if(!err){
        res.render('user/addOredit', {
            viewTitle: "Update User",
            user : doc
        });
    }
});
});

router.get('/delete/:id', (req , res)=> {
User.findByIdAndRemove(req.params.id, (err, doc)=>{
if (!err){
    res.redirect('/user/list');
}
});
});
module.exports = router;